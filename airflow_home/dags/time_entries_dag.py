from datetime import datetime, timedelta, date
import requests
import json
from datetime import datetime, timedelta
import os

import config

from airflow import DAG
from airflow.operators import PythonOperator
from airflow.operators import FetchHarvestJsonOperator
from airflow.operators import HarvestJsonToSqlStagingOperator
from airflow.operators import SqlStagingToBusinessLayerOperator

# Dags default arguments
default_args = {
    'owner': 'Apt-Enginering',
    'depends_on_past': False,
    'retries': 5,
    'retry-delay': timedelta(minutes=1)
}

c = config.config()

dag = DAG(
    dag_id='time_entries_dag',
    description='Grabs all Json from 2018 Harvest projects',
    default_args=default_args,
    start_date=datetime(2018, 2, 17),
    schedule_interval=timedelta(days=7))

headers = {
    'Harvest-Account-ID': '293973',
    'Authorization': 'Bearer 1180.pt.R0fR4V3_C76SWssBnUQo23vE5HTwnr7ai0U2iCpZVVoiWJCaIOHsjafOtYZ8mTdV7EaGLjC_7trAEjfO0RfyCA',
    'User-Agent': 'Harvest API Example',
}

fetch_havest_json = FetchHarvestJsonOperator(headers=c['harvest_endpoint']['headers'], 
                                             url='https://api.harvestapp.com/v2/time_entries',
                                             task_id='fetch_harvest_json', 
                                             execute_date = date.today(),
                                             blob_account_name = c['azure_blob']['account_name'],
                                             blob_account_key = c['azure_blob']['account_key'],
                                             dag=dag)

harvest_json_to_sql_staging = HarvestJsonToSqlStagingOperator(blob_account_name = c['azure_blob']['account_name'], 
                                                              blob_account_key = c['azure_blob']['account_key'],
                                                              mysql_conn_string=c['mysql_connection'],
                                                              pymysql_conn=c['pymysql_connection'],
                                                              task_id='harvest_json_to_sql_staging', dag=dag)

sql_stagin_to_business_layer = SqlStagingToBusinessLayerOperator(pymysql_conn=c['pymysql_connection'],
                                                                 task_id='sql_stagin_to_business_layer', dag=dag)

fetch_havest_json >> harvest_json_to_sql_staging >> sql_stagin_to_business_layer