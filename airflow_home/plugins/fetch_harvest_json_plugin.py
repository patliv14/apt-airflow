from airflow.plugins_manager import AirflowPlugin
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

import requests
import datetime
import json
import logging
from datetime import date

from azure.storage.blob import ContentSettings
from azure.storage.blob import BlockBlobService
from azure.storage.blob import PublicAccess

log = logging.getLogger(__name__)

class FetchHarvestJsonOperator(BaseOperator):
    """
    Fetches all the data available from the harvest api `time_entries` 
    endpoint and writes it to an Azure blob. Every fetch is a full 
    collection of `time_entries` objects and the output file is identifiable
    by the run date. The DAG has this process running weekly.
    """

    @apply_defaults
    def __init__(self, headers, url, blob_account_name, blob_account_key, execute_date, *args, **kwargs):
        self.headers = headers
        self.url = url
        self.blob_account_name = blob_account_name
        self.blob_account_key = blob_account_key
        self.execute_date = execute_date

        super(FetchHarvestJsonOperator, self).__init__(*args, **kwargs)

    def execute(self, context):
        
        response = requests.get(
            self.url + '/?from=2018-01-01/?to=' + str(self.execute_date) + '/', headers=self.headers)

        all_time_entries = []
        block_blob_service = BlockBlobService(
            account_name=self.blob_account_name, account_key=self.blob_account_key)

        block_blob_service.set_container_acl(
            'harvest', public_access=PublicAccess.Container)

        if response.status_code == 200:
            # API response is paginated, need to iterate calls
            data = response.json()
            print('The number of pages in the APi response is: ' + str(data['total_pages']))
            total_pages = data['total_pages']

            # retrieves all time entry json from 2018, 
            # one page at a time
            for page in range(1, total_pages + 1):
                response_paginated = requests.get(self.url + '/?from=2018-01-01/?to=' + str(self.execute_date) + '&page=' + str(page),
                                                  headers=self.headers)

                if response_paginated.status_code == 200:
                    # TODO: this print should be writing to a log
                    print('Getting page ' + str(page) +
                          ' of ' + str(total_pages) + '...')
                    data = response_paginated.json()
                    all_time_entries.append(data)
                else:
                    raise ValueError('Error fetching Api')

            now = datetime.datetime.now()

            block_blob_service.create_blob_from_text(
                'harvest',
                'harvest_time_entries' +
                now.strftime("%Y-%m-%d-%H:%M") + '.json',
                json.dumps(all_time_entries)
            )

        else:
            # TODO: should probably come up with a better error
            return("Error with the API Call")


class FetchHarvestJsonPlugin(AirflowPlugin):
    name = "fetch_harvest_json_plugin"
    operators = [FetchHarvestJsonOperator]