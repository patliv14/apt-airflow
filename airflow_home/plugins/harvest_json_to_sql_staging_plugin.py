from airflow.plugins_manager import AirflowPlugin
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from azure.storage.blob import ContentSettings
from azure.storage.blob import BlockBlobService
from azure.storage.blob import PublicAccess

import json
import pandas as pd
from pandas.io.json import json_normalize
import pymysql
import sqlalchemy
from datetime import datetime


class HarvestJsonToSqlStagingOperator(BaseOperator):
    

    @apply_defaults
    def __init__(self, blob_account_name, blob_account_key, mysql_conn_string, pymysql_conn, blob_date=None, container_name='harvest', data_node='time_entries', *args, **kwargs):
        """Move & transform json from an Azure blob to a MySQL staging table.

        Arguments:
        blob_account_name -- str, name of Azure account
        blob_account_key -- str, the secret key for Azure account
        blob_date -- str, `YYYY-MM-DD-HH:MM` formatted date (default: None)
        mysql_conn_string -- str, a connection string for MySQL
        container_name -- str, name of a blob containter (default 'harvest')
        data_node -- str, the key for getting the JSON data value (default: 'time_entries')
        """
        self.blob_account_name = blob_account_name
        self.blob_account_key = blob_account_key
        self.mysql_conn_string = mysql_conn_string
        self.pymysql_conn = pymysql_conn
        self.blob_date = blob_date # used only for time traveling / backfilling 
        self.container_name = container_name
        self.data_node = data_node # the node of the json resonse json that contains data

        super(HarvestJsonToSqlStagingOperator, self).__init__(*args, **kwargs)

    def execute(self, context):
        # get most recent blob from azure 
        block_blob_service = BlockBlobService(
            account_name=self.blob_account_name, account_key=self.blob_account_key)

        all_blobs = list(map(
            lambda b: {'name': b.name, 'date': datetime.strptime(b.name[-21:-5], '%Y-%m-%d-%H:%M')}, # lambda creates dict of name & date for each blob
            block_blob_service.list_blobs(self.container_name).items)) # 

        if(self.blob_date):
            # only executes when `blob_date` is provided
            given_date = datetime.strptime(self.blob_date, '%Y-%m-%d-%H:%M')
            all_blob_dates = list(map(lambda b: b['date'] ,all_blobs))

            if(given_date in all_blob_dates):
                #TODO: make sure the list returned from filtering is actually length 1...
                blob_to_upload = list(filter(lambda b: b['date'] == given_date, all_blobs))[0] 
            else:
                raise ValueError('The provided `blob_date` does not correspond to any existing blobs')
        else:
            # executes in a normal run 
            blob_to_upload = sorted(
                all_blobs,
                key=lambda d: d['date'],
                reverse=True)[0]  # catches the most recent blob

        blob = block_blob_service.get_blob_to_text(self.container_name, blob_to_upload['name']).content # downloading the `blob_to_upload` lol
        data = json.loads(blob) # a list of json objects
        
        normalized_json = pd.concat(list(map( # concatenate list of pandas dfs into one
            lambda d: json_normalize(d[self.data_node]), # flatten json list into a list pandas dfs
            data)))

        # add indicator to pandas df
        normalized_json['CurrentIndicator'] = 1

        # make column names legal --no dots
        normalized_json.columns = normalized_json.columns.str.replace('.', '_')

        # instantiate mysqlConnection
        conn = pymysql.connect(
            host=self.pymysql_conn['host'],
            user=self.pymysql_conn['username'],
            password=self.pymysql_conn['password'],
            db=self.pymysql_conn['database'])

        try: 
            with conn.cursor() as cursor:
                sql = "UPDATE staging.staging_harvest_time SET CurrentIndicator = 0"
                cursor.execute(sql)
            
            conn.commit()

        finally:
            conn.close()

        # instantiate database engine
        engine = sqlalchemy.create_engine(self.mysql_conn_string)
        # write to database
        normalized_json.to_sql('staging_harvest_time', schema='Staging', con=engine, if_exists='append')

class HarvestJsonToSqlStagingPlugin(AirflowPlugin):
    name = 'harvest_json_to_sql_staging_plugin'
    operators = [HarvestJsonToSqlStagingOperator]

