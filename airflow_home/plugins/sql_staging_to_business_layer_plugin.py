from airflow.plugins_manager import AirflowPlugin
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from azure.storage.blob import ContentSettings
from azure.storage.blob import BlockBlobService
from azure.storage.blob import PublicAccess

import requests
import json
import pandas as pd
import pprint
import pymysql


class SqlStagingToBusinessLayerOperator(BaseOperator):
    """
    Runs a merge statement from MySQL staging
    into MySQL business layer.
    """

    @apply_defaults
    def __init__(self, pymysql_conn, *args, **kwargs):
        self.pymysql_conn=pymysql_conn

        super(SqlStagingToBusinessLayerOperator, self).__init__(*args, **kwargs)

    def execute(self, context):
        conn = pymysql.connect(
            host=self.pymysql_conn['host'],
            user=self.pymysql_conn['username'],
            password=self.pymysql_conn['password'])

        try:
            with conn.cursor() as cursor:
                sql1 = """INSERT INTO airflow.harvest_time  (
                    TimeEntryId,
                    UserName,
                    ClientName,
                    ProjectName,
                    Hours,
                    BillableRate,
                    SpentDate,
                    Notes,
                    TaskName,
                    BillableFlag,
                    InsertDate
                )
                SELECT
                    sta.id,
                    sta.user_name,
                    sta.client_name,
                    sta.project_name,
                    sta.hours,
                    ifnull(sta.billable_rate, 0),
                    sta.spent_date,
                    ifnull(sta.notes, 'none'),
                    sta.task_name,
                    sta.billable,
                    curdate()
                FROM staging.staging_harvest_time  sta
                WHERE CurrentIndicator = 1
                ON DUPLICATE KEY UPDATE
                    UserName = sta.user_name,
                    ClientName = sta.client_name,
                    ProjectName = sta.project_name,
                    Hours = sta.hours,
                    BillableRate = sta.billable_rate,
                    SpentDate = sta.spent_date,
                    Notes = sta.notes,
                    TaskName = sta.task_name,
                    BillableFlag = sta.billable;
                    """
                cursor.execute(sql1)

            conn.commit()

        finally:
            conn.close()

class SqlStagingToBusinessLayerPlugin(AirflowPlugin):
    name = "sql_stagin_to_business_layer_plugin"
    operators = [SqlStagingToBusinessLayerOperator]
