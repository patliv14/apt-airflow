# Aptitive & Airflow

Welcome to the `Apt-Airflow` Repository. Here, you'll find all of the code we've put together in our effort to prove out [Airflow](https://airflow.apache.org/) as an ETL tool and to learn about the platform. 

If you'd like to play with the app we put together, there are some getting started steps you'll need to get through first. 

### Getting Started

Prerequisites for using this repo:

- A functioning `bash` terminal of some kind. 

- Python3 installed on your computer.

  - `virtualenv` and `pip` installed.

- This directory cloned to your computer.

  ​

---



1. Reach out to Patrick or Ainsley for the `config.py`. It has been excluded from this repo for security reasons. Nothing will run without it.

2. Open a terminal at the top level of the directory you cloned. 

   ```
   cd apt-airflow
   ```

3. This application was developed with `virtualenv`, so you'll need one of your own. Run the following line to create your local python environment.

   ```
   virtualenv -p `which python3` venv
   ```

   This creates a directory called `venv` that contains an executable Python3 and all the dependencies of the project. (Run `which python` to double check that it refers to the path you're working in.)

4. Activate your venv by running `source venv/bin/activate`. (Simply run `deactivate` when you're done.)

5. Now, to get dependencies, run `pip install -r requirements.txt`

   This will install the list of python modules in `requirements.txt`. 

6. You should be ready to go. Now we must create tell Airflow where its home is.

   ```
   export AIRFLOW_HOME=`pwd`/airflow_home
   ```

   This creates an environment variable called `AIRFLOW_HOME` in your virtual enviroment. Verify it exists by running `echo $AIRFLOW_HOME`. Path should be something like `files/in/your/computer/apt-airflow/airflow_home`.

7. Now run `airflow version` and you'll see a lovely graphic. This does a bunch of magic, such as creating an `airflow.cfg` file among others. 

8. Now run `airflow initdb`. This creates a local SQLite database for ETL Management data. (This is the OOTB setting but can be changed, don't freak out.)

9. We're nearly to the meat & potatoes. Run `airflow webserver` to serve Airflow's gui on your local machine. You should see something other than a 404 at `localhost:8080`.

10. Finally, open a new terminal (same directory). Repeat steps 3 through 6 and then run `airflow scheduler` to activate the DAG. 

   ​

Congratulations! You have created yourself a sandbox. Go check out the [Airflow docs](https://airflow.apache.org/), read this [tutorial](http://michal.karzynski.pl/blog/2017/03/19/developing-workflows-with-apache-airflow/), or talk to Ainsley & Pat to go further with this tool. 